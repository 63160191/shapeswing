
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class TriangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblBase = new JLabel("base:", JLabel.TRAILING);
        lblBase.setSize(30, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        
        final javax.swing.JTextField txtBase = new javax.swing.JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(50, 5);
        
        JLabel lblHeigth = new JLabel("height:", JLabel.TRAILING);
        lblHeigth.setSize(40, 20);
        lblHeigth.setLocation(5, 30);
        lblHeigth.setBackground(Color.WHITE);
        lblHeigth.setOpaque(true);
        
        final javax.swing.JTextField txtHeigth = new javax.swing.JTextField();
        txtHeigth.setSize(50, 20);
        txtHeigth.setLocation(50, 30);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(5, 60);
        
        final JLabel lblResult = new JLabel("Triangle base=?? height=?? area=?? perimeter=??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setBackground(Color.CYAN);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 100);
        
        frame.add(lblResult);
        frame.add(btnCalculate);
        frame.add(txtHeigth);
        frame.add(lblHeigth);
        frame.add(txtBase);
        frame.add(lblBase);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try { 
                    String strBase = txtBase.getText();
                    double base = Double.parseDouble(strBase);
                    String strHeigth = txtHeigth.getText();
                    double heigth = Double.parseDouble(strHeigth);
                    Triangle triangle = new Triangle(base,heigth);
                    lblResult.setText("Triangle base = " + String.format("%.2f", triangle.getBase())
                                + " heigth = " + String.format("%.2f", triangle.getHeigth())
                                + " area = " + String.format("%.2f", triangle.calArea())
                                + " Perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                }catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number");
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHeigth.setText("");
                    txtHeigth.requestFocus();
                }
            }
        });
                
                
        frame.setVisible(true);
    }
    
}
