/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Triangle extends Shape {
    private double base;
    private double heigth;
    
    public Triangle(double base,double heigth) {
        super("Triangle");
        this.base = base;
        this.heigth = heigth;
    }

    public void setHeigth(double heigth) {
        this.heigth = heigth;
    }

    public void setBase(double width) {
        this.base = base;
    }

    public double getHeigth() {
        return heigth;
    }

    public double getBase() {
        return base;
    }

    @Override
    public double calArea() {
        return 0.5*base*heigth;
    }

    @Override
    public double calPerimeter() {
        double c = Math.pow(heigth, 2)+Math.pow(base, 2);
        return heigth+base+(Math.sqrt(c));
    }
}
