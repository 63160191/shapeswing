
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class RectangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblWide = new JLabel("base:", JLabel.TRAILING);
        lblWide.setSize(30, 20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        
        final javax.swing.JTextField txtWide = new javax.swing.JTextField();
        txtWide.setSize(50, 20);
        txtWide.setLocation(50, 5);
        
        JLabel lblLength = new JLabel("height:", JLabel.TRAILING);
        lblLength.setSize(40, 20);
        lblLength.setLocation(5, 30);
        lblLength.setBackground(Color.WHITE);
        lblLength.setOpaque(true);
        
        final javax.swing.JTextField txtLength = new javax.swing.JTextField();
        txtLength.setSize(50, 20);
        txtLength.setLocation(50, 30);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(5, 60);
        
        final JLabel lblResult = new JLabel("Rectangle wide=?? length=?? area=?? perimeter=??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        lblResult.setLocation(0, 100);
        
        frame.add(lblResult);
        frame.add(btnCalculate);
        frame.add(txtLength);
        frame.add(lblLength);
        frame.add(txtWide);
        frame.add(lblWide);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try { 
                    String strWide = txtWide.getText();
                    double wide = Double.parseDouble(strWide);
                    String strLength = txtLength.getText();
                    double length = Double.parseDouble(strLength);
                    Rectangle rectangle = new Rectangle(wide,length);
                    lblResult.setText("Triangle wide = " + String.format("%.2f", rectangle.getWide())
                                + " length = " + String.format("%.2f", rectangle.getLength())
                                + " area = " + String.format("%.2f", rectangle.calArea())
                                + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                }catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number");
                    txtWide.setText("");
                    txtWide.requestFocus();
                    txtLength.setText("");
                    txtLength.requestFocus();
                }
            }
        });
        frame.setVisible(true);
    }
}
