/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Rectangle extends Shape {
    private double wide;
    private double length;
    
    public Rectangle(double wide, double length) {
        super("Rectangle");
        this.wide = wide;
        this.length = length;
    }

    public double getWide() {
        return wide;
    }

    public double getLength() {
        return length;
    }

    public void setWide(double wide) {
        this.wide = wide;
    }

    public void setLength(double length) {
        this.length = length;
    }
    
    @Override
    public double calArea() {
        return  wide*length;   
    }

    @Override
    public double calPerimeter() {
        return  (2*wide)+(2*length);   
    }
    
    
}
